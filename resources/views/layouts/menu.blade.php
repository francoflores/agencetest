<!--Navbar-->
<nav class="navbar navbar-expand-lg navbar-light blue lighten-5">

  <!-- Navbar brand -->
  <a class="navbar-brand" href="#">
    <img src="images/logo.gif" height="30" alt="mdb logo">
  </a>

  <!-- Collapse button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="basicExampleNav">

    <!-- Links -->
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#"><i class="fas fa-home"></i> Agence
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><i class="fas fa-clipboard-check"></i> Projetos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><i class="fas fa-tasks"></i> Administrativo</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><i class="fas fa-coins"></i> Comercial</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><i class="fas fa-money-check-alt"></i> Financeiro</a>
      </li>

      <!-- Dropdown -->
      <!-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">Dropdown</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li> -->

    </ul>
    <!-- Links -->

    <!-- Links -->
    <ul class="navbar-nav ml-auto">
      <!-- Dropdown -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false"><i class="far fa-user"></i> Usuario (Invitado)</a>
        <div class="dropdown-menu dropdown-menu-right dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">Perfil</a>
          <a class="dropdown-item" href="#">Salir</a>
        </div>
      </li>

    </ul>
    <!-- Links -->

    <!-- <form class="form-inline">
      <div class="md-form my-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
      </div>
    </form> -->
  </div>
  <!-- Collapsible content -->

</nav>
<!--/.Navbar-->