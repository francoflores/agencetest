@extends('layouts.index')

@section('content')
<div class="section mt-2">
    <div class="card">
        <div class="card-header p-1">Filtro</div>
        <div class="card-body p-1">
            <div class="row">
                <div class="col col-sm-12 col-md-6 m-0">
                    <form>
                        <div class="form-row">
                            <div class="col">
                                <label for="idFrom">Desde</label>
                                <input type="month" id="idFrom" value="{{$from}}" class="form-control">
                            </div>
                            <div class="col">
                                <label for="idTo">Hasta</label>
                                <input type="month" id="idTo" value="{{$to}}" class="form-control">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col col-sm-12 col-md-6 text-center mt-2">
                    <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-outline-mdb-color waves-effect"  id="idRelatorio">
                        <i class="fas fa-clipboard-list fa-sm pr-2" aria-hidden="true"></i> Relatório
                        </button>
                        <button type="button" class="btn btn-outline-mdb-color waves-effect" id="idChartLine">
                            <i class="fas fa-chart-line fa-sm pr-2" aria-hidden="true"></i> Gráfico
                        </button>
                        <button type="button" class="btn btn-outline-mdb-color waves-effect" id="idChartPie">
                            <i class="fas fa-chart-pie fa-sm pr-2" aria-hidden="true"></i> Pizza
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section mt-3">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
            aria-selected="true"><i class="fas fa-user-tie"></i> Consultores</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
            aria-selected="false"><i class="fas fa-user-circle"></i> Client</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <select class="browser-default custom-select" id="consultorSelect" multiple>
            @foreach($consultores as $consultor)
                <option value="{{$consultor->getOriginal('co_usuario')}}">{{$consultor->no_usuario}}</option>
            @endforeach
            </select>

            <div class="section mt-1">
                <div class="row d-none"  id="idRowUsersData" >
                    
                </div>
                <div class="row d-none" id="idRowChartBar">
                    <div class="col text-center">
                        <div id="idBarChart" style="width: 900px; height:400px"></div>
                    </div>
                </div>
                <div class="row d-none" id="idRowChartPie">
                    <div class="col text-center">
                        <div id="idPieChart" style="width: 900px; height:400px"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            Clients
        </div>
    </div>
</div>

@endsection

@push('scripts')

<script type="application/javascript">

    function getParams() {
        return {
            from: $('#idFrom').val(),
            to: $('#idTo').val(),
            users: $('#consultorSelect').val()
        }
    }

    function getServerApiUrl() {
        return 'http://localhost:8000/api/';
    }

    function searchRelatori() {
        var params = getParams();

        var url = '{{ route('relatoriApi') }}'; 

        $.post(url, params, function(response) {
            console.log(response);
            setDataUsers(response.dataUsers);
        }, 'json');
    }

    function setDataUsers(data) {
        $('#idRowUsersData').html('');
        for(var i=0;i<data.length;i++) {
            var userData = data[i];

            var html = '<div class="col col-sm-12 col-md-6">';
            html += '<div class="card mb-1">';
            html += '<div class="card-header pt-0 pb-0 text-center">'+userData.no_usuario+'</div>';
            html += '<div class="card-body p-1">';
            html += '<table class="table table-sm mb-1">';
            html += `<thead>
                    <tr>
                        <th scope="col" class="p-0">Período</th>
                        <th scope="col" class="p-0">Receita Líquida</th>
                        <th scope="col" class="p-0">Custo Fixo</th>
                        <th scope="col" class="p-0">Comissão</th>
                        <th scope="col" class="p-0">Lucro</th>
                    </tr>
                    </thead>`;
            html += '<tbody>';

            var sumLiquida = 0;
            var sumCusto = 0;
            var sumComissao = 0;
            var sumLucro = 0;

            for(var j=0;j<userData.months.length;j++) {
                var month = userData.months[j].month;
                var receita_liquida = userData.months[j].receita_liquida;
                var custo_fixo = userData.months[j].custo_fixo;
                var comissao = userData.months[j].comissao;
                var lucro = userData.months[j].lucro;
                html += '<tr>';
                html += '<th scope="row" class="p-0">'+month+'</th>';
                html += '<td class="p-0">'+receita_liquida+'</td>';
                html += '<td class="p-0">'+custo_fixo+'</td>';
                html += '<td class="p-0">'+comissao+'</td>';
                html += '<td class="p-0">'+lucro+'</td>';
                html += '</tr>';

                sumLiquida += receita_liquida;
                sumCusto += custo_fixo;
                sumComissao += comissao;
                sumLucro += lucro;
            }

            html += '<tr class="grey lighten-2">';
            html += '<th scope="row" class="p-0">Saldo</th>';
            html += '<td class="p-0">'+receita_liquida+'</td>';
            html += '<td class="p-0">'+custo_fixo+'</td>';
            html += '<td class="p-0">'+comissao+'</td>';
            html += '<td class="p-0">'+Number(sumLucro).toFixed(2)+'</td>';
            html += '</tr>';


            html += '</tbody>';
            html += '</table>';
            html += '</div>';

            html += '</div>';
            html += '</div>';

            $('#idRowUsersData').append(html);
        }
    }

    var chartBar = echarts.init(document.getElementById('idBarChart'));
    var chartPie = echarts.init(document.getElementById('idPieChart'));

    var optionChartBar = {
        title: {
            text: 'Performance Comercial',
            //subtext: 'Enero 2007 a Mayo 2007',
            textAlign: 'center',
            left: '50%'
        },
        grid: {
            bottom: '150px',
            right: '200px',
            left: '70px',
            top: '50px'
        },
        tooltip: {},
        xAxis: {
            type: 'category',
            data: []
        },
        yAxis: {},
        series: [],

        legend: {
            type: 'scroll',
            orient: 'horizontal',
            bottom: '50px'
        }
    };

    var optionChartPie = {
        title : {
            text: 'Participacao na Receita',
            x:'center'
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },

        legend:{
            type: 'scroll',
            orient: 'vertical',
            right: '-10px'
        },

        series : [
            {
                name: 'Cantidad',
                type: 'pie',
                radius : '55%',
                center: ['40%', '50%'],
                data: [],
                label: {
                    normal: {
                        formatter: '{b} {d}%'
                    }
                },
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };

    function getChartBarData() {
        var url = '{{ route('chartBarApi') }}'; 
        var params = getParams();

        $.post(url, params, function(response) {
            setDataChartBar(response);
        }, 'json');
    }

    function setDataChartBar(data) {
        chartBar.showLoading();
        chartBar.clear();

        optionChartBar.xAxis.data = data.category;
        optionChartBar.series = data.series;
        chartBar.setOption(optionChartBar);
        chartBar.hideLoading();
    }

    function getChartPieData() {
        var url = '{{ route('chartPieApi') }}'; 
        var params = getParams();

        $.post(url, params, function(response) {
            console.log(response);
            setDataChartPie(response);
        }, 'json');
    }

    function setDataChartPie(data) {
        chartPie.showLoading();
        chartPie.clear();

        optionChartPie.series[0].data = data.dataSerie;
        chartPie.setOption(optionChartPie);
        chartPie.hideLoading();
    }

    function showModule(idModule) {
        if(idModule == 'idRowUsersData') {
            if($('#idRowUsersData').hasClass('d-none')) {
                $('#idRowUsersData').removeClass('d-none');
            }
            if(!$('#idRowChartBar').hasClass('d-none')) {
                $('#idRowChartBar').addClass('d-none');
            }
            if(!$('#idRowChartPie').hasClass('d-none')) {
                $('#idRowChartPie').addClass('d-none');
            }
        }
        else if(idModule == 'idRowChartBar') {
            if($('#idRowChartBar').hasClass('d-none')) {
                $('#idRowChartBar').removeClass('d-none');
            }
            if(!$('#idRowUsersData').hasClass('d-none')) {
                $('#idRowUsersData').addClass('d-none');
            }
            if(!$('#idRowChartPie').hasClass('d-none')) {
                $('#idRowChartPie').addClass('d-none');
            }
        }
        else {
            if($('#idRowChartPie').hasClass('d-none')) {
                $('#idRowChartPie').removeClass('d-none');
            }
            if(!$('#idRowUsersData').hasClass('d-none')) {
                $('#idRowUsersData').addClass('d-none');
            }
            if(!$('#idRowChartBar').hasClass('d-none')) {
                $('#idRowChartBar').addClass('d-none');
            }
        }
    }

    $(document).ready(function() {
        $('#consultorSelect').select2({
            placeholder: 'Consultores',
        });

        $('#idRelatorio').click(function() {
            searchRelatori();
            showModule('idRowUsersData');
        });

        $('#idChartLine').click(function() {
            getChartBarData();
            showModule('idRowChartBar');
        });

        $('#idChartPie').click(function() {
            getChartPieData();
            showModule('idRowChartPie');
        });
    });
</script>
@endpush