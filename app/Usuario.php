<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    //
    protected $table = 'cao_usuario';
    protected $primaryKey = 'co_usuario';

    protected $fillable = ["*"];

    public function permissao() {
        return $this->hasMany('App\Permissao', 'co_usuario', 'co_usuario');
    }

    public function os() {
        //return $this->hasMany('App\Os', 'co_usuario', 'co_usuario');
        return Os::where('co_usuario', '=', $this->getOriginal('co_usuario'))->get();
    }
}
