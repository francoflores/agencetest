<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fatura extends Model
{
    protected $table = 'cao_fatura';
    protected $primaryKey = 'co_fatura';

    public function os() {
        return $this->belongsTo('App\Os', 'co_os', 'co_os');
    }
}
