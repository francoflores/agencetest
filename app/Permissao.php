<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permissao extends Model
{
    //
    protected $table = 'permissao_sistema';
    protected $primaryKey = 'co_usuario';
}
