<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usuario;
use App\Permissao;
use App\Os;
use App\Fatura;
use App\Salario;

class ConsultorController extends Controller
{
    //

    public function index() {


        $from = '2007-02-31'; //date('Y-m-d');
        $to = '';

        $to = new \DateTime('2007-02-28');
        $from = (new \DateTime($to->format('Y-m-d')))->sub(new \DateInterval('P1M'));

        $registers = Usuario::whereHas('permissao' , function($query) {
            $query->where('in_ativo', '=', 'S')
            ->where('co_sistema', '=', '1')
            ->whereIn('co_tipo_usuario', [0,1,2]);
        })->get();

        $data = [];

        foreach($registers as $register) {
            array_push($data, ['co_usuario'=>$register->getAttribute('co_usuario'), 'no_usuario'=>$register->no_usuario]);
        }

        return view('desempenho.index', [
            'consultores' => $registers,
            'from' => $from->format('Y-m'),
            'to' => $to->format('Y-m')
        ]);
    }

    public function searchRelatori(Request $request) {

        $from = $request->get('from', '');
        $to = $request->get('to', '');
        $users = $request->get('users', null);

        $months = $this->getMonths($from, $to);

        $registerUsers = Usuario::whereHas('permissao' , function($query) {
            $query->where('in_ativo', '=', 'S')
            ->where('co_sistema', '=', '1')
            ->whereIn('co_tipo_usuario', [0,1,2]);
        })->get();

        $dataUsers = [];

        foreach($registerUsers as $register) {
            $co_usuario = $register->getOriginal('co_usuario');
            if($users != null && !in_array($co_usuario, $users)) {
                continue;
            }

            $os = Os::where('co_usuario', '=', $co_usuario)->select(['co_os'])->get()->toArray();

            $newMonths = [];

            $salario = Salario::where('co_usuario', '=', $co_usuario)->first();

            foreach($months as $month) {
                $start = $month['from'];
                $end = $month['to'];

                $faturas = Fatura::whereIn('co_os',$os)
                            ->where('data_emissao', '>=', $start)
                            ->where('data_emissao', '<=', $end)
                            ->get();
                
                $newMonth = [
                    'from' => $month['from'],
                    'to' => $month['to'],
                    'month' => $month['month']
                ];

                $newMonth['receita_liquida'] = 0;
                $newMonth['custo_fixo'] = $salario == null? 0:$salario->brut_salario;
                $newMonth['comissao'] = 0;
                $newMonth['lucro'] = 0;
                $newMonth['faturas'] = $faturas;
                $newMonth['os'] = $os;
                $newMonth['co_usuario'] = $co_usuario;
                if(count($faturas) > 0) {
                    $newMonth['receita_liquida'] = $faturas->reduce(function($carry, $item) {
                        return $carry + ($item->valor - ($item->valor*$item->total_imp_inc)/100);
                    });

                    $newMonth['comissao'] = $faturas->reduce(function($carry, $item) {
                        return $carry + ($item->valor - ($item->valor*$item->total_imp_inc)/100)*$item->comissao_cn/100;
                    });
                }

                $newMonth['receita_liquida'] = round($newMonth['receita_liquida'], 2);
                $newMonth['comissao'] = round($newMonth['comissao'], 2);

                $newMonth['lucro'] = round($newMonth['receita_liquida'] - ($newMonth['custo_fixo'] + $newMonth['comissao']), 2);

                array_push($newMonths, $newMonth);
            }
            
            array_push($dataUsers, [
                'no_usuario'=> $register->no_usuario, 
                'co_usuario'=> $co_usuario,
                'months' => $newMonths
            ]);
            
        }
        
        return response()->json([
            'from' => $from,
            'to' => $to,
            'users' => $users,
            'dataUsers' => $dataUsers,
            'months' => $months
        ]);
    }

    public function searchDataPie(Request $request) {
        $from = $request->get('from', '');
        $to = $request->get('to', '');
        $users = $request->get('users', null);

        $endMonth = $to == '' ? (new \DateTime()):(new \DateTime($to.'-28'));
        $startMonth = $from == '' ? ((new \DateTime($endDate->format('Y-m-d')))->sub(new \DateInterval('P2M'))):(new \DateTime($from.'-01'));

        $registerUsers = Usuario::whereHas('permissao' , function($query) {
            $query->where('in_ativo', '=', 'S')
            ->where('co_sistema', '=', '1')
            ->whereIn('co_tipo_usuario', [0,1,2]);
        })->get();

        $dataSeriesPie = [];

        foreach($registerUsers as $register) {
            $co_usuario = $register->getOriginal('co_usuario');
            if($users != null && !in_array($co_usuario, $users)) {
                continue;
            }

            $os = Os::where('co_usuario', '=', $co_usuario)->select(['co_os'])->get()->toArray();

            $faturas = Fatura::whereIn('co_os',$os)
                            ->where('data_emissao', '>=', $startMonth->format('Y-m-d'))
                            ->where('data_emissao', '<=', $endMonth->format('Y-m-d'))
                            ->get();
            
            $receita_liquida = $faturas->reduce(function($carry, $item) {
                return $carry + ($item->valor - ($item->valor*$item->total_imp_inc)/100);
            });

            array_push($dataSeriesPie, [
                'name' => $register->no_usuario,
                'value' => round($receita_liquida,2)
            ]);
        }

        return response()->json([
            'dataSerie' => $dataSeriesPie,
            'from' => $endMonth,
            'to' => $startMonth
        ]);
    }

    public function searchDataBar(Request $request) {
        $from = $request->get('from', '');
        $to = $request->get('to', '');
        $users = $request->get('users', null);

        $months = $this->getMonths($from, $to);

        $category = [];
        $series = [];

        foreach($months as $month) {
            array_push($category, $month['month']);
        }

        $registerUsers = Usuario::whereHas('permissao' , function($query) {
            $query->where('in_ativo', '=', 'S')
            ->where('co_sistema', '=', '1')
            ->whereIn('co_tipo_usuario', [0,1,2]);
        })->get();

        $salarioPromedio = 0;

        $seriePromedio = [
            'type' => 'line',
            'name' => 'Custo Fixo Medio',
            'data' => []
        ];

        foreach($registerUsers as $register) {
            $co_usuario = $register->getOriginal('co_usuario');
            if($users != null && !in_array($co_usuario, $users)) {
                continue;
            }

            $data = [];

            $os = Os::where('co_usuario', '=', $co_usuario)->select(['co_os'])->get()->toArray();
            
            $salario = Salario::where('co_usuario', '=', $co_usuario)->first();
            $salarioPromedio += $salario == null? 0:$salario->brut_salario;

            foreach($months as $month) {
                $start = $month['from'];
                $end = $month['to'];
                $faturas = Fatura::whereIn('co_os', $os)
                            ->where('data_emissao', '>=', $start)
                            ->where('data_emissao', '<=', $end)
                            ->get();

                if(count($faturas) > 0) {
                    $receita_liquida = $faturas->reduce(function($carry, $item) {
                        return $carry + ($item->valor - ($item->valor*$item->total_imp_inc)/100);
                    });

                    array_push($data, $receita_liquida);
                }
            }

            array_push($series, [
                'type' => 'bar',
                'name' => $register->no_usuario,
                'data' => $data //[10, 20]
            ]);
            
        }

        $nSeries = count($series);
        $salarioPromedio = $salarioPromedio / $nSeries;
        $seriePromedio['data'] = array_fill(0, count($category), $salarioPromedio);

        array_push($series, $seriePromedio);

        return response()->json([
            'category' => $category,
            'series' => $series
        ]);
    }

    private function getMonths($start = '', $end = '') {
        
        $endDate = $end == '' ? (new \DateTime()):(new \DateTime($end.'-28'));
        $startDate = $start == '' ? ((new \DateTime($endDate->format('Y-m-d')))->sub(new \DateInterval('P2M'))):(new \DateTime($start.'-01'));

        $nextDate = new \DateTime($startDate->format('Y-m-d'));
        $intervals = [];

        $yearBefore = '';
        while($nextDate < $endDate) {
            $year = $nextDate->format('Y');
            array_push($intervals, [
                //'month' => $nextDate->format('F').' de '.$year,
                'month' => $this->getPortugueseMonth($nextDate->format('n')).' de '.$year,
                'from' => $nextDate->format('Y-m-').'01', 
                'to' => $nextDate->format('Y-m-').cal_days_in_month(CAL_GREGORIAN, $nextDate->format('m'), $nextDate->format('Y'))
                ]);
            $nextDate->add(new \DateInterval('P1M'));
            $yearBefore = $year;
        }

        return $intervals;
    }

    private function getPortugueseMonth($month) {
        $months = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dec'];
        return $months[$month-1];
    } 
}
