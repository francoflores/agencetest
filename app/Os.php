<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Os;

class Os extends Model
{
    protected $table = 'cao_os';
    protected $primaryKey = 'co_os';

    public function faturas() {
        return $this->hasMany('App\Fatura', 'co_os', 'co_os');
    }
}
